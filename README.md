# Rayhan's Fronted Boilerplate
use this repo to start project

please install `npm install gulp@latest --save-dev`

## Folder Structure

Here is an directory structure in which source code (src) is separated from temporary precompiled assets (.tmp), which are separated from the final distribution folder (dist).

- tmp : contains compiled js, css and html files
- app : contains higher level languages such as jade, typescript and scss
- dist : contains only concatenated and minified files optimized to be served in production

## Steps

1. install gulp-cli
`npm install -g gulp-cli`
don't use `npm install -g gulp` -> deprecated

2. install npm packages
`npm install`

3. run the program
`npm start`

4. configure the program, html, scss, js

5. build the program
`npm run build`
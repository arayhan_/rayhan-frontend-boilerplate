let $ = require('jquery');
let TypeIt = require('typeit');

$(document).ready(function() {
    new TypeIt('#hero', {
        strings: ['Welcome to my base project...', 'Have Fun!'],
        speed: 80,
        breakLines: false,
        waitUntilVisible: true,
        deleteSpeed: 15
    }).go();
})
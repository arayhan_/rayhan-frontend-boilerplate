// ------------------------------------
// Define Variables
// ------------------------------------

const { src, dest, series, parallel, watch } = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const fileinclude = require('gulp-file-include');
const gulpif = require('gulp-if');
const htmlbeautify = require('gulp-html-beautify');
const htmlmin = require('gulp-htmlmin');
const htmlreplace = require('gulp-html-replace');
const imagemin = require('gulp-imagemin');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const useref = require('gulp-useref');
const uglify = require('gulp-uglify');
const uglifycss = require('gulp-uglifycss');
const webpack = require('webpack-stream');

const PATH_ROOT = {
	APP: './app/',
	TMP: './.tmp/',
	DIST: './dist/'
};

const PATH = {
	APP: {
		ROOT: PATH_ROOT.APP,
		SCSS: PATH_ROOT.APP + 'scss/',
		JS: PATH_ROOT.APP + 'js/',
		IMAGES: PATH_ROOT.APP + 'images/',
		TEMPLATES: PATH_ROOT.APP + 'templates/',
	},
	TMP: {
		ROOT: PATH_ROOT.TMP,
		CSS: PATH_ROOT.TMP + 'css/',
		JS: PATH_ROOT.TMP + 'js/',
		IMAGES: PATH_ROOT.TMP + 'images/',
	},
	DIST: {
		ROOT: PATH_ROOT.DIST,
		CSS: PATH_ROOT.DIST + 'css/',
		JS: PATH_ROOT.DIST + 'js/',
		IMAGES: PATH_ROOT.DIST + 'images/',
	}
}

const CSS_BUILD_FILENAME = 'app.bundle.css';
const JS_BUILD_FILENAME = 'app.bundle.js';
const CSS_MIN_BUILD_FILENAME = 'app.bundle.min.css';
const JS_MIN_BUILD_FILENAME = 'app.bundle.min.js';

// ------------------------------------
// HTML Task
// ------------------------------------
function html() {
	let htmlFile = '*.html'

	return src(PATH.APP.ROOT + htmlFile)
	.pipe(fileinclude())
	.pipe(htmlbeautify())
	.pipe(dest(PATH.TMP.ROOT))
}

// ------------------------------------
// SCSS Task
// ------------------------------------

function scss() {
	let scssFile = 'app.scss';

	return src(PATH.APP.SCSS + scssFile)
	.pipe(sourcemaps.init())
	.pipe(plumber())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer('last 2 versions'))
	.pipe(sourcemaps.write('./'))
	.pipe(plumber.stop())
	.pipe(dest(PATH.TMP.CSS))
	.pipe(browserSync.stream())
}

// ------------------------------------
// Javascript Task
// ------------------------------------

function js() {
	let jsFile = 'app.js';
	let webpackFile = './webpack.config.js';

	return src(PATH.APP.JS + jsFile)
	.pipe(sourcemaps.init())
	.pipe(webpack(require(webpackFile)))
	.pipe(sourcemaps.write('./'))
	.pipe(dest(PATH.TMP.JS))
}

// ------------------------------------
// Compress Images Task
// ------------------------------------

function compressimages() {
	let imageFile = '**/*'

	return src(PATH.APP.IMAGES + imageFile)
	.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
				plugins: [
					{removeViewBox: true},
					{cleanupIDs: false}
				]
			})
		]))
	.pipe(dest(PATH.TMP.IMAGES))
}

// ------------------------------------
// Serve Task
// ------------------------------------

function serve() {
	let htmlFile = '**/*.html';
	let scssFile = '**/*.scss';
	let imagesFile = '**/*';

	browserSync.init({
		server: {
			baseDir: PATH.TMP.ROOT,
		}
	})

	watch(PATH.APP.SCSS + scssFile, scss)
	watch(PATH.APP.ROOT + htmlFile, html).on('change', browserSync.reload)
	watch(PATH.APP.ROOT + imagesFile, compressimages).on('change', browserSync.reload)
}

// ------------------------------------
// Build Task
// ------------------------------------

function buildhtml() {
	let htmlFile = '*.html';

	return src(PATH.TMP.ROOT + htmlFile)
	.pipe(htmlmin({collapseWhitespace: true}))
	.pipe(htmlreplace({
			css: 'css/' + CSS_BUILD_FILENAME,
			js: 'js/' + JS_BUILD_FILENAME,
		}))
	.pipe(dest(PATH.DIST.ROOT))
}

function buildcss() {
	let cssFile = 'app.css';

	return src(PATH.TMP.CSS + cssFile)
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(uglifycss())
	.pipe(rename(CSS_BUILD_FILENAME))
	.pipe(sourcemaps.write('./'))
	.pipe(dest(PATH.DIST.CSS))
}

function buildjs() {
	let jsFile = 'app.js';
	
	return src(PATH.TMP.JS + jsFile)
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(uglify())
	.pipe(rename(JS_BUILD_FILENAME))
	.pipe(sourcemaps.write('./'))
	.pipe(dest(PATH.DIST.JS))
}

function buildcompressimages() {
	let imageFile = '**/*'
	
	return src(PATH.TMP.IMAGES + imageFile)
	.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
				plugins: [
					{removeViewBox: true},
					{cleanupIDs: false}
				]
			})
		]))
	.pipe(dest(PATH.DIST.IMAGES))
}

exports.html = html;
exports.scss = scss;
exports.js = js;
exports.compressimages = compressimages;
exports.buildhtml = series(html, buildhtml);
exports.buildcss = series(scss, buildcss);
exports.buildjs = series(js, buildjs);
exports.buildcompressimages = series(buildcompressimages);
exports.serve = serve;
exports.build = series(
	parallel(html, scss, js, compressimages),
	parallel(buildhtml, buildcss, buildjs, buildcompressimages)
);